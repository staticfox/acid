/*
 * Copyright (c) 2016, orillion <orillion@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.plugins.vizon;

import net.rizon.acid.plugins.vizon.db.VizonDatabase;
import net.rizon.acid.plugins.vizon.db.VizonUser;
import net.rizon.acid.plugins.vizon.db.VizonDrawing;
import com.google.common.eventbus.Subscribe;
import io.netty.util.concurrent.ScheduledFuture;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;
import net.rizon.acid.conf.Config;
import net.rizon.acid.conf.ConfigException;
import net.rizon.acid.core.AcidUser;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.User;
import net.rizon.acid.events.EventJoin;
import net.rizon.acid.events.EventNickChange;
import net.rizon.acid.events.EventSync;
import net.rizon.acid.events.EventUserMode;
import net.rizon.acid.plugins.Plugin;
import net.rizon.acid.plugins.vizon.commands.PendingRequestCommand;
import net.rizon.acid.plugins.vizon.conf.VizonConfig;
import net.rizon.acid.plugins.vizon.util.VizonTemporal;
import net.rizon.acid.sql.SQL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author orillion <orillion@rizon.net>
 */
public class Vizon extends Plugin
{
	private static final Logger logger = LoggerFactory.getLogger(Vizon.class);

	private static VizonConfig conf;
	private static AcidUser vizonBot;
	private static Channel vizonChannel;
	
	private static SQL vizonSql;
	private static VizonDatabase vizonDatabase;
	
	private static SQL vizonThreadSql;
	private static VizonDatabase vizonThreadDatabase;
	
	private static RequestTracker requestTracker;
	private static VhostManager vhostManager;
	private static DrawGenerator generator;

	private ScheduledFuture checkDrawingFuture;
	private ScheduledFuture vhostExpiryFuture;
	
	private LotteryThread lotteryThread;

	public static VizonConfig getConf()
	{
		return conf;
	}

	public static AcidUser getVizonBot()
	{
		return vizonBot;
	}

	public static Channel getVizonChannel()
	{
		return vizonChannel;
	}

	public static SQL getVizonSql()
	{
		return vizonSql;
	}

	public static VizonDatabase getVizonDatabase()
	{
		return vizonDatabase;
	}

	public static VizonDatabase getVizonThreadDatabase()
	{
		return vizonThreadDatabase;
	}

	public static RequestTracker getRequestTracker()
	{
		return requestTracker;
	}

	public static VhostManager getVhostManager()
	{
		return vhostManager;
	}

	public static DrawGenerator getGenerator()
	{
		return generator;
	}

	public static void updateChannel(VizonDatabase database)
	{
		for (String nick : vizonChannel.getUsers())
		{
			VizonUser user = database.findUser(nick);

			if (user == null)
			{
				continue;
			}

			if (user.isPermanent())
			{
				// Grand prize winner
				Acidictive.setMode(
					vizonBot.getNick(),
					vizonChannel.getName(),
					String.format("+h %s", user.getNick()));
			}
			else if (user.getObtained() != null)
			{
				// User won a vhost
				Acidictive.setMode(
					vizonBot.getNick(),
					vizonChannel.getName(),
					String.format("+v %s", user.getNick()));
			}
		}
	}

	private void checkDrawing()
	{
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime drawingDate = VizonTemporal.determineNextDrawing(now, conf);

		VizonDrawing drawing = vizonDatabase.findEarliestUnrunDrawing();

		if (drawing == null)
		{
			drawing = vizonDatabase.createDrawing(drawingDate);

			if (drawing == null)
			{
				logger.error("Error creating new drawing");
				return;
			}
		}

		logger.debug("Next drawing will be at {}, now is {}", drawing.getDate(), now);
		
		if (drawing.getDate().isAfter(now))
		{
			return;
		}
		
		if (lotteryThread != null && lotteryThread.isAlive())
		{
			logger.warn("Unable to start new drawing thread, old one is still alive");
			return;
		}
		
		lotteryThread = new LotteryThread(drawing);
		lotteryThread.start();
	}

	@Override
	public void start() throws Exception
	{
		reload();

		vizonSql = SQL.getConnection("vizon");
		vizonThreadSql = SQL.getConnection("vizon");
		vizonDatabase = new VizonDatabase(vizonSql);
		vizonThreadDatabase = new VizonDatabase(vizonThreadSql);
		requestTracker = new RequestTracker(Acidictive.conf);
		vhostManager = new VhostManager();
		generator = new DrawGenerator();

		checkDrawingFuture = Acidictive.scheduleAtFixedRate(this::checkDrawing, 1, TimeUnit.MINUTES);
		vhostExpiryFuture = Acidictive.scheduleAtFixedRate(vhostManager::expireVhosts, 1, TimeUnit.DAYS);

		Acidictive.eventBus.register(this);

		// Load all pending requests from the DB.
		requestTracker.getPending();
	}

	@Subscribe
	public void onNickChange(EventNickChange event)
	{
		User user = event.getU();

		if (!user.isIdentified())
		{
			return;
		}

		vhostManager.applyVhostIfApplicable(user);
		setChannelModeIfApplicable(user);
	}

	@Subscribe
	public void onModeChange(EventUserMode event)
	{
		User user = event.getUser();

		if (!event.getNewmodes().contains("r"))
		{
			return;
		}

		vhostManager.applyVhostIfApplicable(user);
		setChannelModeIfApplicable(user);
	}

	@Subscribe
	public void onChannelJoin(EventJoin event)
	{
		Channel channel = event.getChannel();

		if (!vizonChannel.equals(channel))
		{
			return;
		}

		for (User user : event.getUsers())
		{
			setChannelModeIfApplicable(user);
		}
	}

	private void setChannelModeIfApplicable(User user)
	{
		if (!user.isIdentified())
		{
			return;
		}

		VizonUser vu = Vizon.getVizonDatabase().findUser(user.getNick());

		if (vu == null || vu.getObtained() == null)
		{
			return;
		}

		String mode = vu.isPermanent() ? "+h" : "+v";

		Acidictive.setMode(
			vizonBot.getNick(),
			vizonChannel.getName(),
			String.format(
				"%s %s",
				mode,
				user.getNick()));
	}

	@Subscribe
	public void onSync(EventSync event)
	{
		// Run pending command on start.
		PendingRequestCommand c = new PendingRequestCommand();
		String channel = Acidictive.conf.getChannelNamed("vhost");
		Channel chan = Channel.findChannel(channel);
		
		if (chan == null)
			return;
		
		c.Run(vizonBot, vizonBot, chan, new String[0]);

		Vizon.updateChannel(vizonDatabase);
	}

	@Override
	public void stop()
	{
		if (checkDrawingFuture != null)
		{
			checkDrawingFuture.cancel(true);
			checkDrawingFuture = null;
		}
		
		if (vhostExpiryFuture != null)
		{
			vhostExpiryFuture.cancel(true);
			vhostExpiryFuture = null;
		}
		
		vizonSql.shutdown();
		vizonThreadSql.shutdown();

		Acidictive.eventBus.unregister(this);
	}

	@Override
	public void reload() throws Exception
	{
		conf = (VizonConfig) Config.load("vizon.yml", VizonConfig.class);
		Acidictive.loadClients(this, conf.clients);

		User u = User.findUser(conf.vizonBot);

		if (u == null || !(u instanceof AcidUser))
		{
			throw new ConfigException("VizonBot not loaded, or its nickname is in use by someone else");
		}

		Channel channel = Channel.findChannel(conf.vizonChannel);

		if (channel == null)
		{
			throw new ConfigException("VizonChannel does not exist");
		}

		vizonBot = (AcidUser) u;

		if (!vizonBot.isOnChan(channel))
		{
			throw new ConfigException("VizonBot not in Vizon channel");
		}

		vizonChannel = channel;
	}
}
