/*
 * Copyright (c) 2017, orillion <orillion@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.plugins.vizon;

import net.rizon.acid.plugins.vizon.db.VizonDatabase;
import net.rizon.acid.plugins.vizon.db.VizonUser;
import net.rizon.acid.plugins.vizon.db.VizonDrawing;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import static java.util.stream.Collectors.toList;
import net.rizon.acid.core.AcidUser;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.User;
import net.rizon.acid.util.Format;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author orillion <orillion@rizon.net>
 */
public class LotteryThread extends Thread
{
	private static final Logger logger = LoggerFactory.getLogger(LotteryThread.class);

	private AcidUser vizonUser;
	private Channel channel;
	private VizonDatabase database;
	private VizonDrawing drawing;

	private final List<VizonUser> jackpockWinners = new ArrayList<>();
	private final List<VizonUser> grandPrizeWinners = new ArrayList<>();
	private final List<VizonUser> firstPrizeWinners = new ArrayList<>();
	private final List<VizonUser> secondPrizeWinners = new ArrayList<>();
	private final List<VizonUser> thirdPrizeWinners = new ArrayList<>();
	private final List<VizonUser> consolationPrizeWinners = new ArrayList<>();

	public LotteryThread(VizonDrawing drawing)
	{
		super("LotteryThread");
		this.drawing = drawing;
	}

	@Override
	public void run()
	{
		database = Vizon.getVizonThreadDatabase();
		channel = Vizon.getVizonChannel();
		vizonUser = Vizon.getVizonBot();

		boolean channelWasModerated = channel.hasMode('m');

		List<VizonBet> bets = database.findBetsForDrawing(drawing);

		DrawGenerator generator = Vizon.getGenerator();
		Bet winning;

		if (drawing.getId() % 100 == 0)
		{
			// Do special drawing
			List<Bet> userBets = bets.stream()
					.map((b) -> b.getBet())
					.collect(toList());

			winning = generator.generateSpecial(userBets);
		}
		else
		{
			// Do normal drawing
			winning = generator.generate();
		}

		// Set the results of the drawing.
		drawing.setDrawingResult(winning);

		// Announce we're starting the drawing
		privmsgChannel(String.format(
				"Starting drawing No.%d of VIzon...",
				drawing.getId()));

		if (!channelWasModerated)
		{
			// No need to set +m, channel was already moderated
			Acidictive.setMode(
					vizonUser.getUID(),
					channel.getName(),
					"+m");
		}

		// Announce a new number every 5 seconds.
		for (int i = 1; i <= 6; i++)
		{
			try
			{
				Thread.sleep(5000);

				privmsgChannel(String.format(
						"%s number is %2d",
						ordinal(i),
						winning.asList().get(i - 1)));
			}
			catch (InterruptedException ex)
			{
				logger.error("LotteryRunner thread was interrupted");
				return;
			}
		}

		try
		{
			Thread.sleep(5000);
		}
		catch (InterruptedException ex)
		{
			logger.error("LotteryRunner thread was interrupted");
			return;
		}

		// Sort the winning numbers from low to high.
		List<Integer> winningNumbers = winning.asList();
		Collections.sort(winningNumbers);

		// Announce results of the drawing, ordered.
		privmsgChannel(String.format(
				"The result of No.%d drawing of VIzon is %d %d %d %d %d %d",
				drawing.getId(),
				winningNumbers.get(0),
				winningNumbers.get(1),
				winningNumbers.get(2),
				winningNumbers.get(3),
				winningNumbers.get(4),
				winningNumbers.get(5)));

		// Determine winners
		for (VizonBet bet : bets)
		{
			int correct = drawing.checkCorrect(bet.getBet());

			switch (correct)
			{
				case 6:
					// First prize
					handleFirstPrize(bet);
					break;
				case 5:
					// Second prinze
					handleSecondPrize(bet);
					break;
				case 4:
					// Third prize
					handleThirdPrize(bet);
					break;
				case 3:
					// consolation prize
					handleConsolationPrize(bet);
					break;
				default:
					// No prize
					break;
			}
		}

		// Announce we have grand prize winners, if there are any, else
		// ignore it.
		if (grandPrizeWinners.size() > 0)
		{
			privmsgChannel(String.format(
					"%cGrand prize: %s%c",
					Format.BOLD,
					winnersString(grandPrizeWinners.size()),
					Format.BOLD));
		}

		// Announce number of first prize winners.
		privmsgChannel(String.format(
				"%s prize: %s",
				ordinal(1),
				winnersString(firstPrizeWinners.size())));

		// Announce number of second prize winners.
		privmsgChannel(String.format(
				"%s prize: %s",
				ordinal(2),
				winnersString(secondPrizeWinners.size())));

		// Announce number of third prize winners.
		privmsgChannel(String.format(
				"%s prize: %s",
				ordinal(3),
				winnersString(thirdPrizeWinners.size())));

		// Announce number of consolation prize winners.
		privmsgChannel(String.format(
				"Consolation prize: %s",
				winnersString(consolationPrizeWinners.size())));

		// Announce number of people that participated
		privmsgChannel(String.format(
				"No. of bets placed: %d",
				bets.size()));

		if (!channelWasModerated)
		{
			// Channel was not +m before the drawing began, so we need to reset
			// it to -m
			Acidictive.setMode(
					vizonUser.getUID(),
					channel.getName(),
					"-m");
		}

		// Process and notify all winners
		processWinners();

		database.updateDrawing(drawing);

		Vizon.updateChannel(database);
	}

	private void processWinners()
	{
		for (VizonUser user : jackpockWinners)
		{
			notifyJackpotWinner(user);
		}

		for (VizonUser user : grandPrizeWinners)
		{
			notifyGrandPrizeWinner(user);
		}

		for (VizonUser user : firstPrizeWinners)
		{
			notifyFirstPrizeWinner(user);
		}

		for (VizonUser user : secondPrizeWinners)
		{
			notifySecondPrizeWinner(user);
		}

		for (VizonUser user : thirdPrizeWinners)
		{
			notifyThirdPrizeWinner(user);
		}

		for (VizonUser user : consolationPrizeWinners)
		{
			notifyConsolationPrizeWinner(user);
		}
	}

	private void notifyJackpotWinner(VizonUser user)
	{
		User u = User.findUser(user.getNick());

		notifyUser(
				u,
				String.format("Congratulations, you won the jackpot in VIzon "
						+ "No.%d! You are eligible to select a new vhost and "
						+ "won a 1 letter nickname!", drawing.getId()),
				user.getNick());
	}

	private void notifyGrandPrizeWinner(VizonUser user)
	{
		User u = User.findUser(user.getNick());

		notifyUser(
				u,
				String.format("Congratulations, you won the grand prize in VIzon "
						+ "No.%d! You are eligible to select a new vhost! Your "
						+ "vhost will now be permanent!", drawing.getId()),
				user.getNick());
	}

	private void notifyFirstPrizeWinner(VizonUser user)
	{
		User u = User.findUser(user.getNick());

		notifyUser(
				u,
				String.format("Congratulations, you won the first prize in VIzon "
						+ "No.%d! You are eligible to select a new vhost! And your "
						+ "vhost will be made bold!", drawing.getId()),
				user.getNick());
	}

	private void notifySecondPrizeWinner(VizonUser user)
	{
		User u = User.findUser(user.getNick());

		notifyUser(
				u,
				String.format("Congratulations, you won the second prize in VIzon "
						+ "No.%d! You are eligible to select a new vhost!", drawing.getId()),
				user.getNick());
	}

	private void notifyThirdPrizeWinner(VizonUser user)
	{
		User u = User.findUser(user.getNick());

		notifyUser(
				u,
				String.format("Congratulations, you won the third prize in VIzon "
						+ "No.%d! You are eligible to select a new colored vhost!", drawing.getId()),
				user.getNick());
	}

	private void notifyConsolationPrizeWinner(VizonUser user)
	{
		User u = User.findUser(user.getNick());

		notifyUser(
				u,
				String.format("Congratulations, you won the consolation prize in VIzon "
						+ "No.%d! You are eligible to select a new colored vhost!", drawing.getId()),
				user.getNick());
	}

	private void notifyUser(User user, String message, String nick)
	{
		Acidictive.privmsg(
				Vizon.getVizonBot().getNick(),
				"MemoServ",
				String.format("SEND %s %s", nick, message));

		if (user != null && user.isIdentified())
		{
			Acidictive.privmsg(
					Vizon.getVizonBot().getNick(),
					nick,
					message);
		}
	}

	private static String winnersString(int winners)
	{
		switch (winners)
		{
			case 0:
				return "No winners";
			case 1:
				return "1 winner";
			default:
				return String.format("%d winners", winners);
		}
	}

	private void handleFirstPrize(VizonBet bet)
	{
		VizonUser user = database.findUserById(bet.getUserId());

		if (user == null)
		{
			return;
		}

		if (user.getObtained() == null)
		{
			user.setObtainedId(bet.getDrawingId());
			user.setObtained(LocalDateTime.now());
		}

		// Allow user to request a new vhost
		user.setEligible(true);
		// Set the user's vhost to bold
		user.setBold(true);

		// @TODO: Determine if we are actually going to use this in the future.
//		if (user.isPermanent() && !user.isJackpot() && false)
//		{
//			// User has won grand prize before, and is eligible for the jackpot
//			jackpockWinners.add(user);
//		}
//		else
		{
			// Run normal checks
			int multiplier = user.getMultiplier();

			user.addDays(Vizon.getConf().firstPrize * multiplier);

			if (user.getDays() >= 180 && !user.isPermanent())
			{
				// User won grand prize!
				// Set +h on user
				user.setPermanent(true);
				grandPrizeWinners.add(user);
			}
			else
			{
				// Set +v on user.
				firstPrizeWinners.add(user);
			}
		}

		user.incrementMultiplier();
		database.updateUser(user);
	}

	private void handleSecondPrize(VizonBet bet)
	{
		VizonUser user = database.findUserById(bet.getUserId());

		if (user == null)
		{
			return;
		}

		if (user.getObtained() == null)
		{
			user.setObtainedId(bet.getDrawingId());
			user.setObtained(LocalDateTime.now());
		}

		// Allow user to request a new vhost
		user.setEligible(true);

		int multiplier = user.getMultiplier();

		user.addDays(Vizon.getConf().secondPrize * multiplier);

		if (user.getDays() >= 180 && !user.isPermanent())
		{
			// User won grand prize!
			// Set +h on user
			user.setPermanent(true);
			grandPrizeWinners.add(user);
		}
		else
		{
			// Set +v on user.
			secondPrizeWinners.add(user);
		}

		user.incrementMultiplier();
		database.updateUser(user);
	}

	private void handleThirdPrize(VizonBet bet)
	{
		VizonUser user = database.findUserById(bet.getUserId());

		if (user == null)
		{
			return;
		}

		if (user.getObtained() == null)
		{
			user.setObtainedId(bet.getDrawingId());
			user.setObtained(LocalDateTime.now());
		}

		// Allow user to request a new vhost
		user.setEligible(true);

		int multiplier = user.getMultiplier();

		user.addDays(Vizon.getConf().thirdPrize * multiplier);

		if (user.getDays() >= 180 && !user.isPermanent())
		{
			// User won grand prize!
			// Set +h on user
			user.setPermanent(true);
			grandPrizeWinners.add(user);
		}
		else
		{
			// Set +v on user.
			thirdPrizeWinners.add(user);
		}

		user.incrementMultiplier();
		database.updateUser(user);
	}

	private void handleConsolationPrize(VizonBet bet)
	{
		VizonUser user = database.findUserById(bet.getUserId());

		if (user == null)
		{
			return;
		}

		if (user.getObtained() == null)
		{
			user.setObtainedId(bet.getDrawingId());
			user.setObtained(LocalDateTime.now());
		}

		// Allow user to request a new vhost
		user.setEligible(true);

		user.addDays(Vizon.getConf().consolationPrize);

		if (user.getDays() >= 180 && !user.isPermanent())
		{
			// User won grand prize!
			// Set +h on user
			user.setPermanent(true);
			grandPrizeWinners.add(user);
		}
		else
		{
			// Set +v on user.
			consolationPrizeWinners.add(user);
		}

		database.updateUser(user);
	}

	private void privmsgChannel(String message)
	{
		Acidictive.privmsg(
				vizonUser.getUID(),
				channel.getName(),
				message);
	}

	private static String ordinal(int i)
	{
		String[] sufixes = new String[]
		{
			"th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th"
		};
		switch (i % 100)
		{
			case 11:
			case 12:
			case 13:
				return i + "th";
			default:
				return i + sufixes[i % 10];

		}
	}
}
