import urllib2
import map
import feed
import threading
import war
from feed import XmlFeed, get_json
from datetime import datetime, timedelta
from decimal import Decimal

cache = {}
lock = threading.Lock()

def get_battle(battle_id):
	with lock:
#		try:
			if battle_id in cache:
				battle = cache[battle_id]
				if not battle.active:
					return battle
				
				if battle.erep_api_fails and battle.refresh_api_feed():
					return battle
				
				if (datetime.now() - battle.status['timestamp']).seconds <= 6:
					return cache[battle_id]
				
				cache[battle_id].update()
				return cache[battle_id]
			else:
				battle = Battle(battle_id)
				cache[battle_id] = battle
				return battle
#		except Exception as e:
#			raise e

class Battle:
	def __init__(self, battle_id):
		battle_feed = XmlFeed('http://api.erepublik.com/v2/feeds/battles/%d' % battle_id)
		self.id = battle_id
		self.finished_reason = battle_feed.text('/battle/progress/finished-reason')
		self.finished_at = battle_feed.text('/battle/progress/finished-at')
		if self.finished_at:
			self.finished_at = datetime.strptime(self.finished_at[:-6], '%Y-%m-%dT%H:%M:%S') - timedelta(hours=3)
		
		self.started_at = battle_feed.text('/battle/progress/started-at')
		if self.started_at:
			self.started_at = datetime.strptime(self.started_at[:-6], '%Y-%m-%dT%H:%M:%S') - timedelta(hours=3)
			
		self.active = True if not self.finished_at else False
		self.defender = {'id': battle_feed.int('/battle/defender/id'),
						'name': battle_feed.text('/battle/defender/name'),
						'code': battle_feed.text('/battle/defender/code')}
		self.attacker = {'id': battle_feed.int('/battle/attacker/id'),
						'name': battle_feed.text('/battle/attacker/name'),
						'code': battle_feed.text('/battle/attacker/code')}
#		war_id = battle_feed.int('/battle/war/id')
#		war_feed = XmlFeed('http://api.erepublik.com/v1/feeds/war/%d' % war_id)
		self.region = {'id': battle_feed.int('/battle/region/id'),
					'name': battle_feed.text('/battle/region/name')}
		self.is_resistance = battle_feed.bool('/battle/is-resistance')
		self.winner_id = battle_feed.int('/battle/winner')
		
		if self.active:
			self.update_status()
		else:
			self.status = None
			
		self.erep_api_fails = False

	def update_status(self):
		feed = get_json('http://www.erepublik.com/en/military/battle-log/%d' % self.id)
		attpoints = int(feed['points']['attacker_points'])
		defpoints = int(feed['points']['defender_points'])
		domination = feed['domination']
		if domination:
			domination = round(Decimal(domination), 2)
		else:
			self.winner_id = self.defender['id'] if defpoints > attpoints else self.attacker['id']
			self.erep_api_fails = True
		
		self.status = {'domination': domination,
				'attacker_points': attpoints,
				'defender_points': defpoints,
				'elapsed_time': self.get_time(attpoints + defpoints),
				'timestamp': datetime.now()}

	def update(self):
		self.status['previous_check'] = self.status['timestamp']
		self.status['previous_wall'] = self.status['domination']
		self.update_status()
	
	def refresh_api_feed(self):
		battle_feed = XmlFeed('http://api.erepublik.com/v2/feeds/battles/%d' % self.id)
		self.winner_id = battle_feed.int('/battle/winner')
		self.finished_at = battle_feed.text('/battle/progress/finished-at')
		if self.winner_id and self.finished_at:
			self.finished_at = datetime.strptime(self.finished_at[:-6], '%Y-%m-%dT%H:%M:%S') - timedelta(hours=3)
			self.active = False
			return True
		else:
			return False
		
	def get_time(self, wallpoints):
		if wallpoints <= 300:
				r = wallpoints / 10
		elif wallpoints <= 900:
				r = 30 + (wallpoints - 300) / 20
		elif wallpoints <= 1800:
				r = 60 + (wallpoints - 900) / 30
		else:
				r = 90 + (wallpoints - 1800) / 60
		return int(r)
